/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx       = 2;   /* border pixel of windows */
static const unsigned int snap           = 32;  /* snap pixel */
static const unsigned int gappih         = 6;   /* horiz inner gap between windows */
static const unsigned int gappiv         = 6;   /* vert inner gap between windows */
static const unsigned int gappoh         = 4;   /* horiz outer gap between windows and screen edge */
static const unsigned int gappov         = 6;   /* vert outer gap between windows and screen edge */
static const int smartgaps               = 0;   /* 1 means no outer gap when there is only one window */
static const int showbar                 = 1;   /* 0 means no bar */
static const int topbar                  = 1;   /* 0 means bottom bar */
static const int horizpadbar             = 2;   /* horizontal padding for statusbar */
static const int vertpadbar              = 0;   /* vertical padding for statusbar */
static const int focusonnetactive        = 1;   /* 0 means default behaviour, 1 means auto-focus on urgent window */
static const int attachmode              = 0;   /* 0 master (default), 1 = above, 2 = aside, 3 = below, 4 = bottom */
static const int pertag                  = 1;   /* 0 means global layout across all tags (default), 1 = layout per tag (pertag) */
static const int pertagbar               = 1;   /* 0 means using pertag, but with the same barpos, 1 = normal pertag */
static const int zoomswap                = 1;   /* 0 means default behaviour, 1 = zoomswap patch */
static const int fancybar                = 0;   /* 0 means default behaviour, 1 = fancybar patch */
static const int savefloats              = 1;   /* 0 means default behaviour, 1 = savefloats patch */
static const int losefullscreen          = 1;   /* 0 means default behaviour, 1 = losefullscreen patch */
static const int nrg_force_vsplit        = 1;   /* nrowgrid layout, 1 means force 2 clients to always split vertically */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray             = 1;   /* 0 means no systray */
static const char *fonts[]               = { "agave Nerd Font Mono:size=10" };
static const char dmenufont[]            =   "agave Nerd Font Mono:size=10";
static const char col_gray1[]            =   "#000000";
static const char col_orange[]           =   "#8787d7";
static const char col_gray[]             =   "#2b2b2b";
static const char col_black[]            =   "#000000";
static const char col_pink[]             =   "#8787d7";
static const char col_red[] 		 =   "#ff6b68";
static const unsigned int baralpha       =         255;
static const unsigned int borderalpha    =         255;
static const char *colors[][3] = {
	/*                   fg         bg      border   */
	[SchemeNorm] = { col_black,  col_pink,  col_pink },
	[SchemeSel]  = { col_orange, col_gray,  col_red  },
};
static const unsigned int alphas[][3] = {
	/*                   fg         bg      border   */
	[SchemeNorm] = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]  = { OPAQUE, baralpha, borderalpha },
};

/* tagging */
static const char *tags[] = { "A", "B", "C", "D", "E", "F" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 *  WM_WINDOW_ROLE(STRING) = role
	 */
	/* class            		role       instance    title        tags mask    switchtag  iscentered   isfloating   isfakefullscreen monitor */
	{ "Gimp",			NULL,	   NULL,       NULL,	    0,		 1,	    0,		 0,	      0,	       -1 },
	{ "qutebrowser",		NULL,	   NULL,       NULL,	    0,		 1,	    0,		 0,	      1,	       -1 },
	{ "Joplin",			NULL,	   NULL,       NULL,	    0,		 1,	    0,		 0,	      0,	       -1 },
	{ "kdenlive",			NULL,	   NULL,       NULL,	    0,		 1,	    0,		 0,	      0,	       -1 },
	{ "kitty",			NULL,	   NULL,       NULL,	    0,		 1,	    0,		 0,	      0,	       -1 },
	{ "mpv",			NULL,	   NULL,       NULL,	    0,		 1,	    0,		 0,	      0,	       -1 },
	{ "Virt-manager",	 	NULL,	   NULL,       NULL,	    0,		 1,	    0,		 0,	      1,	       -1 },
	{ "SimpleScreenRecorder",	NULL,	   NULL,       NULL,	    0,		 1,	    0,		 0,	      0,	       -1 },
/*	{ "libreoffice",                NULL,      NULL,       NULL,        1 << 5,      1,         0,           0,           0,               -1 },
	{ "dolphin",                    NULL,      NULL,       NULL,        1 << 4,      1,         0,           0,           0,               -1 },
	{ "LibreWolf",                  NULL,      NULL,       NULL,        1 << 3,      1,         0,           0,           1,               -1 },    */
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* kalo gak 0 st nya gak filling screen, 1 means respect size hints in tiled resizals */

#include "vanitygaps.c"

static const int layoutaxis[] = { 
	1,    /* layout axis: 1 = x, 2 = y; negative values mirror the layout, setting the master area to the right / bottom instead of left / top */
	2,    /* master axis: 1 = x (from left to right), 2 = y (from top to bottom), 3 = z (monocle) */
	2,    /* stack axis:  1 = x (from left to right), 2 = y (from top to bottom), 3 = z (monocle) */
};

static const Layout layouts[] = {
      /* symbol     arrange function */
	{ "<",       tile }, /* first entry is default */
	{ "^",       bstack },
	{ "<=",      deck },
	{ "::",      nrowgrid },
	{ "><",      centeredmaster },
	{ "~",       NULL },
	{ "[0]",     monocle },
	{  NULL,     NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },
#define STACKKEYS(MOD,ACTION) \
	{ MOD, XK_j,     ACTION##stack, {.i = INC(-1) } }, \
	{ MOD, XK_k,     ACTION##stack, {.i = INC(+1) } }, \
	{ MOD, XK_grave, ACTION##stack, {.i = PREVSEL } }, \
	{ MOD, XK_1,     ACTION##stack, {.i = 1 } }, \
	{ MOD, XK_2,     ACTION##stack, {.i = 2 } }, \
	{ MOD, XK_3,     ACTION##stack, {.i = 3 } }, \
	{ MOD, XK_4,     ACTION##stack, {.i = 4 } }, \
	{ MOD, XK_5,     ACTION##stack, {.i = 5 } }, \
	{ MOD, XK_6,     ACTION##stack, {.i = 6 }  },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2]            = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]      = { "dmenu_run", NULL };
static const char *termcmd[]       = { "st", NULL };
static const char *layoutmenu_cmd  = "layoutmenu.sh";
static const char *volup[]         = { "pulsemixer", "--change-volume", "5", NULL };
static const char *voldown[]       = { "pulsemixer", "--change-volume", "-5", NULL  };
static const char *volmute[]       = { "pulsemixer", "--toggle-mute", NULL  };
static const char *brightdown[]    = { "xbacklight", "-5", NULL  };
static const char *brightup[]      = { "xbacklight", "+5", NULL  };
static const char *redshifto[]     = { "rson", NULL };
static const char *redshiftx[]     = { "rsnorm", NULL  };
static const char *ranger[]        = { "st", "-e", "ranger", "/home/gusiev/", NULL  };
static const char *calc[]          = { "sh", "/home/gusiev/.config/mine/calc/dmenu_calc", NULL  };
static const char *clipmenu[]      = { "clipmenu", NULL  };

#include "shiftview.c"
static Key keys[] = {
	/* modifier                      key                 function                 argument */
	{ MODKEY,      		        XK_p,            togglealwaysontop,	{0}  },
	{ NULL,                         XK_F12,          spawn,          	{.v = volup }  },
	{ NULL,                         XK_F11,          spawn,          	{.v = voldown }   },
	{ NULL,                         XK_F10,          spawn,          	{.v = volmute }   },
	{ NULL,                         XK_F5,           spawn,          	{.v = brightdown }   },
	{ NULL,                         XK_F6,           spawn,          	{.v = brightup }   },
	{ NULL,                         XK_F7,           spawn,          	{.v = redshifto }   },
	{ MODKEY,                       XK_F7,           spawn,          	{.v = redshiftx }   },
	{ MODKEY,                       XK_r,            spawn,          	{.v = ranger }   },
	{ NULL,                         XK_F1,           spawn,          	{.v = calc }   },
	{ NULL,                         XK_F2,           spawn,          	{.v = clipmenu }   },
	{ NULL,                         XK_Menu,         spawn,          	{.v = dmenucmd } },
	{ MODKEY,	                XK_Return,       spawn,          	{.v = termcmd } },
	{ MODKEY,                       XK_b,            togglebar,      	{0} },
/*	{ Mod1Mask,                     XK_k,            focusstack,     	{.i = +1 } }, */
/*	{ Mod1Mask,                     XK_j,            focusstack,     	{.i = -1 } },	  cycle window mimic alt+tab */
	STACKKEYS(Mod1Mask,                          focus)
/*	STACKKEYS(MODKEY|ShiftMask,                push) */

	{ MODKEY|ShiftMask,             XK_j,            rotatestack,    	{.i = -1 } },
	{ MODKEY,	                XK_Tab,          rotatestack,    	{.i = +1 } },	/* cycle window to master mimic alt+tab */
	{ MODKEY|ShiftMask,             XK_equal,        incnmaster,     	{.i = +1 } },	/* increase client on master area */
	{ MODKEY|ShiftMask,             XK_minus,        incnmaster,     	{.i = -1 } },	/* decrease client on master area */
	{ MODKEY,	                XK_semicolon,    setcfact,    		{.f =  0.00} },
	{ MODKEY,		        XK_k,	         setcfact,	   	{.f = +0.01} },
	{ MODKEY,		        XK_j,	         setcfact,	   	{.f = -0.01} },
	{ MODKEY,                       XK_h,            setmfact,       	{.f = -0.01} },
	{ MODKEY,                       XK_l,            setmfact,       	{.f = +0.01} },
	{ Mod1Mask,                     XK_Return,       zoom,           	{0} },		/* swap client to master area */
	{ MODKEY,	                XK_equal,        incrgaps,       	{.i = +1 } },	/* increase gaps */
	{ MODKEY,    			XK_minus,        incrgaps,       	{.i = -1 } },	/* decrease gaps */
	{ MODKEY,    			XK_BackSpace,    defaultgaps,    	{0} },		/* reset gaps */
	{ MODKEY,                       XK_c,            killclient,     	{0} },
	{ MODKEY,                       XK_t,            setlayout,      	{.v = &layouts[0]} },
	{ MODKEY,                       XK_y,            setlayout,      	{.v = &layouts[1]} },
	{ MODKEY,			XK_u,	         setlayout,	   	{.v = &layouts[5]} },
	{ MODKEY,                       XK_i,            setlayout,             {.v = &layouts[6]}  },
	{ MODKEY,                       XK_space,        shiftview,      	{.i = +1 } },
	{ MODKEY|Mod1Mask,              XK_space,        shiftview,      	{.i = -1 } },
	{ MODKEY|ShiftMask,             XK_f,	         togglefloating, 	{0} },
	{ MODKEY,                       XK_f,            togglefullscreen, 	{0} },
	{ MODKEY,                       XK_0,            view,           	{.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,            tag,            	{.ui = ~0 } },
	{ MODKEY, 	                XK_comma,        cyclelayout,    	{.i = -1 } },
	{ MODKEY,	                XK_period,       cyclelayout,    	{.i = +1 } },
	TAGKEYS(                        XK_1,            0)
	TAGKEYS(                        XK_2,            1)
	TAGKEYS(                        XK_3,            2)
	TAGKEYS(                        XK_4,            3)
	TAGKEYS(                        XK_5,            4)
	TAGKEYS(                        XK_6,            5)
	TAGKEYS(                        XK_7,            6)
	TAGKEYS(                        XK_8,            7)
	TAGKEYS(                        XK_9,            8)
	{ MODKEY|ControlMask|ShiftMask, XK_q,            quit,           	{0} },
/*	{ MODKEY|ShiftMask,             XK_BackSpace, quit,        {0}  }, */
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        layoutmenu,     {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

